﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlackOutScreen : MonoBehaviour 
{
	private Image blackoutImage;

	void Start () 
	{
		blackoutImage = gameObject.GetComponent <Image>();
		blackoutImage.enabled = false;
	}

	public void blackoutEnableDisable ()
	{
		blackoutImage.enabled = !blackoutImage.enabled;
	}
}
