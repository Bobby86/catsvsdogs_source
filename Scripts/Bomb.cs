﻿using UnityEngine;
using System.Collections;

public class Bomb : Weapon 
{
	public float xSpeed;

	public override void Start () 
	{
		base.Start ();
		ySpeed = gameController.getBombYSpeed ();
		thisRigidbody.velocity = transform.up * ySpeed;
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		GameObject localOther = other.gameObject;
		if (localOther.tag == "laser")
		{
			Destroy (gameObject);
			Destroy (localOther);
		}
		else if (localOther.tag == "grumpy")
		{
			Destroy (gameObject);
			//decrease cat's HP and flash him awhile
			gameController.updateLives (damage);
			localOther.GetComponent <CatDog>().scream ();
			localOther.GetComponent <CatDog>().flashThisObject (4, 0.05f);
		}
	}
}
