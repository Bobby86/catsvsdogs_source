﻿using UnityEngine;
using System.Collections;

public class CatDog : MonoBehaviour {

//	public int health;
	public float shotingDelay;
	public AudioClip onHitSound;

	public bool cantShot = false; // if enemy got damaged, use in Laser script

	protected bool canShot = false;
	protected bool isAlive = true;
	protected bool faceDirection;	//left - true, right - false
	protected float shotingTimer;
	protected float halfWidth;
	protected AudioSource audioSource;
	protected Rigidbody2D thisRigidbody;
	protected BoxCollider2D thisCollider;
	protected SpriteRenderer thisSprite;
	protected GameController gameController;

	public virtual void Start ()
	{
		thisRigidbody = GetComponent<Rigidbody2D> ();
		thisCollider = GetComponent<BoxCollider2D>();
		thisSprite = GetComponent<SpriteRenderer>();
		audioSource = GetComponent <AudioSource>();

		halfWidth = thisCollider.size.x / 6f;
		faceDirection = true; // left direction
		shotingTimer = shotingDelay;

		GameObject gameControllerObj = GameObject.FindWithTag ("GameController");
		if (null != gameControllerObj)
		{
			gameController = gameControllerObj.GetComponent <GameController>();
		}
		else if (null == gameControllerObj)
		{
			Debug.Log ("Cannot find 'MyGameController' object");
		}	
	}

	public virtual void Update ()
	{
		if (!cantShot) // til enemy wasn't damaged
		{
			shoting (); // change canShot state by the timer
		}
		if (canShot) 
		{
			shot ();
		}
	}

	protected void shoting ()
	{
		shotingTimer -= Time.deltaTime;
		if (0 >= shotingTimer)
		{
			shotingTimer = shotingDelay;
			canShot = true;
		}
	}

	public virtual void shot()
	{
	}

	//woof or meow sound when damage taken
	public virtual void scream ()
	{
		audioSource.PlayOneShot (onHitSound);
	}

	//change isAlive state
	public void changeState ()
	{
		isAlive = false;
	}

	public void flashThisObject (int nBlinks, float blinkDelay)
	{
		StartCoroutine (delay (nBlinks, blinkDelay));
	}

	IEnumerator delay (int nBlinks, float blinkDelay)
	{
		for (int i = 0; i < nBlinks * 2; ++i)
		{
			thisSprite.enabled = !thisSprite.enabled;
			yield return new WaitForSeconds(blinkDelay);
		}
		thisSprite.enabled = true;
	}

	public void death ()
	{
		Destroy (gameObject);
	}
}
