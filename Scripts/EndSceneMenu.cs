﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndSceneMenu : PauseMenu 
{
	private bool victory;	//indicates game over or victory, GO as default

	public Button continueOrReplayButton;
	public Text continueOrReplayText;
	
	public override void Start () 
	{
		base.Start ();

		continueOrReplayButton = continueOrReplayButton.GetComponent <Button>();
		continueOrReplayText = continueOrReplayText.GetComponent <Text>();
	}

	//change GO or victory state
	public void setState (bool wonOrNot)
	{
		victory = wonOrNot;
		if (victory)
		{
			if (0 == (Application.levelCount - 1 - Application.loadedLevel))
			{
				continueOrReplayText.text = "";
				continueOrReplayButton.enabled = false;
			}
			else
			{
				continueOrReplayButton.enabled = true;
				continueOrReplayText.text = "NEXT";
			}
		}
		else
		{
			continueOrReplayButton.enabled = true;
			continueOrReplayText.text = "REPLAY";
		}
	}
	
	public void continuePressed ()
	{
		if (victory)
		{
			StartCoroutine (waitToNext());	//w8 1 sec to fade the scene and load next lvl 
		} 
		else
		{
			StartCoroutine (waitToReplay());	//w8 1 sec to fade the scene and load this lvl
		}
	}
	
	IEnumerator waitToNext ()
	{
		yield return new WaitForSeconds(1);	

		if (0 != (Application.levelCount - 1 - Application.loadedLevel))
		{
			Application.LoadLevel (Application.loadedLevel + 1);
		}
		else 	//last scene has been played
		{
			Application.LoadLevel (0);
		}
	}

	IEnumerator waitToReplay ()
	{
		yield return new WaitForSeconds(1);	
		Application.LoadLevel (Application.loadedLevel);
	}
}
