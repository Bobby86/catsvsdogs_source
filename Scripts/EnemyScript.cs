﻿using UnityEngine;
using System.Collections;

public class EnemyScript : CatDog {

	private float xSpeed;
	public float ySpeed;

	public override void Start ()
	{
		base.Start ();
		base.shotingDelay = gameController.getEnemyShootingDelay ();
		base.shotingTimer = base.shotingDelay;
		xSpeed = gameController.getEnemyXSpeed();
	}
	
	public override void Update () 
	{
		enemyMovement();
		base.Update();
	}

	void enemyMovement ()
	{
		float dist = (transform.position - Camera.main.transform.position).z;
		
		float leftBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0, dist)).x + halfWidth;
		float rightBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,0, dist)).x - halfWidth;
		float localXSpeed = -xSpeed;

		if (transform.position.x < leftBorder && faceDirection)	//left border reached and code below doesn't applied yet
		{
			transform.eulerAngles = new Vector2 (0, 180);
			localXSpeed = xSpeed;
			faceDirection = false;
		} 
		if (transform.position.x > rightBorder && !faceDirection) //the same with another direction
		{
			transform.eulerAngles = new Vector2 (0, 0);
			localXSpeed = - xSpeed;
			faceDirection = true;
		}
		transform.Translate (localXSpeed * Time.deltaTime, ySpeed * Time.deltaTime, 0);
	}
	
	void OnBecameInvisible()
	{
		if (null != gameController && !gameController.getGameState ())
			gameController.SendMessage("addScore", 1);
		Destroy (gameObject, 0.2f);	//this <- lowerCamelCase
	}

	public override void shot ()
	{
		canShot = false;
		GameObject bombObj = Instantiate (Resources.Load ("squareBomb", typeof(GameObject))) as GameObject;
		Vector3 localOffset = new Vector3(0, -25 * Time.deltaTime, 0);
		bombObj.transform.position = transform.position + localOffset;
	}
}