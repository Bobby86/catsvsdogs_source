﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {

	public Transform[] spawnPoints; //left - 1, right - 0
	public GameObject enemy;

	private bool newSpawn = true; 
	private bool cantSpawn = false;

	void Start ()
	{
		Random.seed = 42;
	}

	void Update () 
	{
		if (null == GameObject.FindWithTag ("doge") && !cantSpawn && newSpawn)
		{
			newSpawn = false;
			spawn ();
		}

		if (cantSpawn)
		{
			GameObject enemyObject = GameObject.FindWithTag ("doge");
			if (null != enemyObject)// && null != gameControllerObj)
			{
				enemyObject.GetComponent <CatDog>().death ();
			}

			GameObject bombObject = GameObject.FindWithTag ("rainbowBomb");
			if (null != bombObject)// && null != gameControllerObj)
			{
				bombObject.GetComponent <Weapon>().destroyWeapon ();
			}
			
			GameObject laserObject = GameObject.FindWithTag ("laser");
			if (null != laserObject)// && null != gameControllerObj)
			{
				laserObject.GetComponent <Weapon>().destroyWeapon ();
			}
		}
	}

	void spawn ()
	{
		StartCoroutine (w8AndSpawn (0.5f));
	}

	IEnumerator w8AndSpawn (float secondsToWait)
	{
		yield return new WaitForSeconds (secondsToWait);
		int index = Random.Range (0, spawnPoints.Length);
		Instantiate (enemy, spawnPoints[index].position, spawnPoints[index].rotation);
		newSpawn = true;
	}

	// clear active game objects (characters and weapon)
	public void stopSpawn ()
	{
		cantSpawn = true;
	}
}
