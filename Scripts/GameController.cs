﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

	public int winScore;
	private int lives;	

	public float enemyXSpeed;
	public float bombYSpeed;
	public float enemyShootingDelay;

	public Text scoreText;
	public Text livesText;
	public Text gameOverText;
	
	public GameObject blackout;		//blackout effect used when the scene is finished
	public GameObject pauseMenu;
	private bool canGamePause = true;
	public GameObject endSceneMenu;	

//	public SharedScenesData sharedScenesData;

	public GameObject backgroundAudio;
	public GameObject pauseOrEndSceneAudio;
	private AudioSource bgAudio;
	private AudioSource endSceneAudio;

	private int score = 0;
	private int startSceneScore;
	private bool finishOrPlay = false; // is the scene played

	void Start () 
	{
		if (0 == SharedScenesData.playerLives)
		{
			SharedScenesData.playerLives = 9;
		}

		score = SharedScenesData.score;
		startSceneScore = SharedScenesData.score;
		lives = SharedScenesData.playerLives;
		scoreText.text = " X " + score.ToString(); 
		livesText.text = lives.ToString() + " X ";

		endSceneMenu.SetActive (false);
		pauseMenu.SetActive (false);

		bgAudio = backgroundAudio.GetComponent <AudioSource>();
		endSceneAudio = pauseOrEndSceneAudio.GetComponent <AudioSource>();
		endSceneAudio.Stop ();
	}
	
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (canGamePause)
			{
				Time.timeScale = 0;
				//				AudioListener.volume = 0.0f;
				canGamePause = false;
				pauseMenu.SetActive (true);

				bgAudio.Pause ();
				endSceneAudio.Play ();
			}
			else
			{
				Time.timeScale = 1;
				//				AudioListener.volume = 1.0f;
				canGamePause = true;
				pauseMenu.SetActive (false);
				endSceneAudio.Stop ();
				bgAudio.UnPause ();
			}
			blackout.SendMessage ("blackoutEnableDisable");
		}
	}
	
	public bool getGameState ()
	{
		return finishOrPlay;
	}

	public void addScore (int addValue)
	{
		score += addValue;
		scoreText.text = " X " + score.ToString(); 

		if (winScore == score - startSceneScore)
		{
			finishOrPlay = true;
			SharedScenesData.playerLives = lives;
			SharedScenesData.score = score;
			gameFinish ("GOOD! Doge failed!", true);

			GameObject spawnObject = GameObject.FindWithTag ("Respawn");
			if (null != spawnObject)
			{
				spawnObject.GetComponent <EnemySpawn>().stopSpawn ();
			}	
			return;
		}

		if ((score > 0) && (score % 2 == 0))
		{
			if (enemyXSpeed < 3.5f)
			{
				enemyXSpeed += 0.2f;
			}
			if (bombYSpeed > -3f)
			{
				bombYSpeed -= 0.2f;
			}
			if (enemyShootingDelay > 0.2f)
			{
				enemyShootingDelay -= 0.1f;
			}
		}
	}

	public float getEnemyXSpeed ()
	{
		return enemyXSpeed;
	}

	public float getBombYSpeed ()
	{
		return bombYSpeed;
	}

	public float getEnemyShootingDelay ()
	{
		return enemyShootingDelay;
	}

	public void updateLives (int val)
	{
		lives += val;
		livesText.text = lives.ToString() + " X ";
		if (0 >= lives)
		{
			finishOrPlay = true;
			GameObject catObject = GameObject.FindWithTag ("grumpy");
			GameObject spawnObject = GameObject.FindWithTag ("Respawn");
			if (null != catObject && null != spawnObject)
			{
				//catObject.GetComponent <CatDog>().changeState();
				catObject.SendMessage ("changeState");
				spawnObject.GetComponent <EnemySpawn>().stopSpawn ();
			}	

			livesText.color = Color.red;
			SharedScenesData.playerLives = 9;
			SharedScenesData.score = 0;
			gameFinish ("Epic Failure! Now dogs are dominating", false);
		}
	}
	
// GameOver and Victory part
	// 2nd arg indicates victory=1 or failure=0
	void gameFinish (string message, bool won)
	{
		blackout.SendMessage ("blackoutEnableDisable");

		endSceneMenu.SetActive (true);
//		endSceneMenu.GetComponent <EndSceneMenu>().victory = won;
		endSceneMenu.GetComponent <EndSceneMenu>().setState (won);

		if (0.0f != AudioListener.volume)
		{
			StartCoroutine (audioFadeout (0.0f));
		}

		if (Screen.width < gameOverText.rectTransform.sizeDelta.x)
		{
			gameOverText.rectTransform.sizeDelta = new Vector2(Screen.width, gameOverText.rectTransform.sizeDelta.y);
		}

		gameOverText.text = message;
	}

	IEnumerator audioFadeout (float arg)
	{
		float t = arg;
		while (t < 1.0f)
		{
			AudioListener.volume = Mathf.Lerp (1.0f, 0.0f, t);
			yield return null;
			t += Time.deltaTime;
		}
		endSceneAudio.Play ();
		bgAudio.Stop ();
		AudioListener.volume = 1.0f;
	}
}
