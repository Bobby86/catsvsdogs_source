﻿using UnityEngine;
using System.Collections;

public class Laser : Weapon 
{
	public override void Start () 
	{
		base.Start ();
		thisRigidbody.velocity = transform.up * ySpeed;
//		damage = -1;
	}
	
	// not used
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "doge")
		{
//			Debug.Log("YYYYYYY");
			other.attachedRigidbody.isKinematic = false;
			//TODO
			other.isTrigger = false;
			thisCollider.isTrigger = false;
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D (Collision2D other)
	{
		if (other.gameObject.tag == "doge")
		{
			CatDog catDogObj = other.gameObject.GetComponent <CatDog>();
//			catDogObj.cantShot = true;	//no fun at all
			catDogObj.scream ();
			Destroy (gameObject);
		}
	}
}
