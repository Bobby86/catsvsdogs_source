﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour 
{
	public Button quitMainMenuButton;
	public Button soundButton;
	public Button quitButton;

	protected ColorBlock soundDefaultColors;	
	protected ColorBlock soundDisabledColors;
	protected bool soundOn = true;
	
	public virtual void Start () 
	{
		soundButton = soundButton.GetComponent <Button>();
		quitButton = quitButton.GetComponent <Button>();
		quitMainMenuButton = quitMainMenuButton.GetComponent <Button>();

		soundDefaultColors = soundButton.colors;
		soundDisabledColors = soundButton.colors;
		soundDisabledColors.normalColor = soundButton.colors.disabledColor;
		soundDisabledColors.highlightedColor = soundButton.colors.disabledColor;

		if (0 == AudioListener.volume)
		{
			soundButton.colors = soundDisabledColors;
			soundOn = false;
		}
	}
	
	public void soundPressed ()
	{
		soundOn = !soundOn;
		if (soundOn) 	//sound enabled
		{
			soundButton.colors = soundDefaultColors;	
			AudioListener.volume = 1.0f;
		}
		else 	//sound disabled
		{
			soundButton.colors = soundDisabledColors;
			AudioListener.volume = 0.0f;
		}
	}

	public void mainMenuPressed ()
	{
		Time.timeScale = 1;
		StartCoroutine (levelLoad (0));	// w8 1sec for fadeOut
	}
	
	IEnumerator levelLoad (int sceneNumber)
	{
		yield return new WaitForSeconds (1f);
		Application.LoadLevel (sceneNumber);
	}

	public void quitPressed ()
	{
		Time.timeScale = 1;
		StartCoroutine (appQuit ());	//w8 1 sec for fadeOut
	}
	
	IEnumerator appQuit ()
	{
		yield return new WaitForSeconds (1f);
		AudioListener.volume = 0;
		Application.Quit ();	
	}	
}
