﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerScript : CatDog 
{
	public float stepSize;
	private bool shootingEye = true; //central x position eye, false +- x offset

	public override void Update () 
	{
		if (isAlive)
		{
			playerMovement ();
			restrictMovement ();
			base.Update();
		}
		else
		{
			catIsDead();
		}
	}

	void playerMovement ()
	{
		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			if (faceDirection)
			{
				transform.eulerAngles = new Vector2 (0, 180);
				faceDirection = false;
			}
			transform.Translate (Vector2.left * stepSize * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			if (!faceDirection)
			{
				transform.eulerAngles = new Vector2 (0, 0);
				faceDirection = true;
			}
			transform.Translate (Vector2.left * stepSize * Time.deltaTime);
		}
	}

	//borders of movement for player (x axis)
	void restrictMovement ()
	{
		float dist = (transform.position - Camera.main.transform.position).z;

		float leftBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0, dist)).x + halfWidth;
		float rightBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,0, dist)).x - halfWidth;

		transform.position = new Vector3(
			Mathf.Clamp (transform.position.x, leftBorder, rightBorder),
			transform.position.y, transform.position.z 
		);
	}

	public override void shot ()
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			canShot = false;
			Vector3 localOffset = new Vector3(0,0,0);	//spawn offset
			GameObject redLaserObj = Instantiate (Resources.Load ("redLaser", typeof(GameObject))) as GameObject;
			redLaserObj.transform.position = transform.position;

			if (shootingEye) //central spawn position
			{
				localOffset.x = 0f;
				shootingEye = false;
			} 
			else
			{
				if (faceDirection) // -x offset to the left eye
				{
					localOffset.x = -0.25f;
				}
				else	// +x to the right
				{
					localOffset.x = 0.25f;
				}
				shootingEye = true;
			}

			localOffset.y = 0.25f;
			redLaserObj.transform.position = transform.position + localOffset;

			if (faceDirection)
			{
				redLaserObj.transform.eulerAngles = new Vector3 (-30.0f, 30.0f, 0);
			}
			else 
			{
				redLaserObj.transform.eulerAngles = new Vector3 (30.0f, 30.0f, 0);
			}
		}
	}
	
	void catIsDead ()
	{
		Destroy (gameObject, 0.9f);
	}
}
