﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour {

	//all the buttons
	public Button play;
	public Button sound;
	public Button quit;

	private ColorBlock soundDefaultColors;	
	private ColorBlock soundDisabledColors;
	private bool soundOn = true;

	void Start () 
	{
		play = play.GetComponent <Button>();
		sound = sound.GetComponent <Button>();
		quit = quit.GetComponent <Button>();

		soundDefaultColors = sound.colors;
		soundDisabledColors = sound.colors;
		soundDisabledColors.normalColor = sound.colors.disabledColor;
		soundDisabledColors.highlightedColor = sound.colors.disabledColor;

		if (0 == AudioListener.volume)
		{
			sound.colors = soundDisabledColors;
			soundOn = false;
		}
	}
	
	public void soundPressed ()
	{
		soundOn = !soundOn;
		if (soundOn) 	//sound enabled
		{
			sound.colors = soundDefaultColors;	
			AudioListener.volume = 1.0f;
		}
		else 	//sound disabled
		{
			sound.colors = soundDisabledColors;
			AudioListener.volume = 0.0f;
		}
	}

	public void playPressed ()
	{
		SharedScenesData.playerLives = 9;
		SharedScenesData.score = 0;
		StartCoroutine (levelLoad (Application.loadedLevel + 1));	// w8 1sec for fadeOut
	}

	IEnumerator levelLoad (int sceneNumber)
	{
		yield return new WaitForSeconds (1f);
		Application.LoadLevel (sceneNumber);
	}
	
	public void quitPressed ()
	{
		StartCoroutine (appQuit ());	//w8 1 sec for fadeOut
	}

	IEnumerator appQuit ()
	{
		yield return new WaitForSeconds (1f);
		AudioListener.volume = 0;
		Application.Quit ();	
	}
}
