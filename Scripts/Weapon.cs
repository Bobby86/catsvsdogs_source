﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{
	protected Rigidbody2D thisRigidbody;
	protected BoxCollider2D thisCollider;
	protected GameController gameController;
	protected AudioSource audioSource;

	public float ySpeed;
	public int damage;
	public AudioClip spawnSound;

	public virtual void Start () 
	{
		thisRigidbody = GetComponent <Rigidbody2D>();
		thisCollider = GetComponent <BoxCollider2D>();
		audioSource = GetComponent <AudioSource>();

		thisRigidbody.velocity = transform.up * ySpeed;
		audioSource.PlayOneShot (spawnSound);

		GameObject gameControllerObj = GameObject.Find ("MyGameController");
		if (null != gameControllerObj)
		{
			gameController = gameControllerObj.GetComponent <GameController>();
		}
		else if (null == gameControllerObj)
		{
			Debug.Log ("Cannot find 'MyGameController' object");
		}	
	}

	//play a sound when spawned
	public virtual void playSpawnSound ()
	{
		audioSource.PlayOneShot (spawnSound);
	}
	
	protected void OnBecameInvisible()
	{
		Destroy (gameObject);	//this <- lowerCamelCase
	}

	public void destroyWeapon ()
	{
		Destroy (gameObject);
	}
}
